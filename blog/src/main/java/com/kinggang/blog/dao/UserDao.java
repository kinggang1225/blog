package com.kinggang.blog.dao;

import com.kinggang.blog.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author KingGang
 * @className UserDao
 * @description
 * @date 2020/9/17 10:14
 */
@Repository
public interface UserDao extends JpaRepository<User, Long> {

    User findByUsernameAndPassword(String username, String password);
}
