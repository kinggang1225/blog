package com.kinggang.blog.dao;

import com.kinggang.blog.entity.Comment;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author KingGang
 * @className CommentDao
 * @description
 * @date 2020/10/1 15:30
 */
public interface CommentDao extends JpaRepository<Comment, Long> {

    //根据blogId 查询并且父类评论为空 --------root节点
    List<Comment> findByBlogIdAndParentCommentNull(Long blogId, Sort sort);

}
