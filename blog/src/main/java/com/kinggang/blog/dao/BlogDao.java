package com.kinggang.blog.dao;

import com.kinggang.blog.entity.Blog;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author KingGang
 * @className BlogDao
 * @description
 * @date 2020/9/17 16:26
 */
@Repository
public interface BlogDao extends JpaRepository<Blog, Long>, JpaSpecificationExecutor<Blog> {

    @Query("select  b from Blog  b where b.recommend = true ")
    List<Blog> listTopBlog(Pageable pageable);

    @Query("select b from Blog  b where b.title like ?1 or b.content like ?1 ")
    Page<Blog> searchList(String query, Pageable pageable);

    @Modifying
    @Query("update Blog b set b.views = b.views+1 where b.id=?1 ")
    int updateViews(Long id);

    @Query("select function('date_format',b.updateTime,'%Y') as year from Blog b group by function('date_format',b.updateTime,'%Y') order by year desc ")
    List<String> getYearGroup();


    @Query("select b from  Blog b where function('date_format',b.updateTime,'%Y')=?1")
    List<Blog> getBlogByYear(String year);

}
