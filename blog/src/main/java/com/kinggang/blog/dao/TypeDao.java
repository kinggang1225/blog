package com.kinggang.blog.dao;

import com.kinggang.blog.entity.Type;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author KingGang
 * @className TypeDao
 * @description
 * @date 2020/9/17 11:49
 */
@Repository
public interface TypeDao extends JpaRepository<Type, Long> {

    Type findByName(String name);

    //根据分页条件查询分类
    @Query("select t from Type  t ")
    List<Type> findTypeTop(Pageable pageable);
}
