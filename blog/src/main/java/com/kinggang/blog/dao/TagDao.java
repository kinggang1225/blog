package com.kinggang.blog.dao;

import com.kinggang.blog.entity.Tag;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author KingGang
 * @className TypeDao
 * @description
 * @date 2020/9/17 11:49
 */
@Repository
public interface TagDao extends JpaRepository<Tag, Long> {

    Tag findByName(String name);

    @Query("select  t from Tag t")
    List<Tag> findTagTop(Pageable pageable);
}
