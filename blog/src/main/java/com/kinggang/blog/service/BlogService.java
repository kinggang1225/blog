package com.kinggang.blog.service;

import com.kinggang.blog.entity.Blog;
import com.kinggang.blog.vo.BlogQuery;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * @author KingGang
 * @className BlogService
 * @description
 * @date 2020/9/17 16:22
 */
public interface BlogService {

    Blog getBlog(Long id);

    Blog getAndConvert(Long id);

    Page<Blog> listBlog(Pageable pageable, BlogQuery blog);

    List<Blog> listRecommendBlogTop(Integer size);

    Blog saveBlog(Blog blog);

    Blog updateBlog(Long id, Blog blog);

    void deleteBlog(Long id);

    Page<Blog> listBlog(Pageable pageable);

    Page<Blog> searchList(String query, Pageable pageable);

    Page<Blog> listBlog(Long id, Pageable pageable);

    Map<String, List<Blog>> showArchive();

    int blogCount();
}
