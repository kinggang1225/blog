package com.kinggang.blog.service;

import com.kinggang.blog.entity.Type;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * @author KingGang
 * @className TypeService
 * @description
 * @date 2020/9/17 11:46
 */
public interface TypeService {

    Type saveType(Type type);

    Type getType(Long id);

    Type getTypeByName(String name);

    Page<Type> listTypeTop(Pageable pageable);

    List<Type> listTypeTop(Integer size);

    Type updateType(Long id, Type type);

    void deleteType(Long id);

    List<Type> listTypeTop();
}
