package com.kinggang.blog.service;

import com.kinggang.blog.entity.Tag;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * @author KingGang
 * @className TagService
 * @description
 * @date 2020/9/17 11:46
 */
public interface TagService {

    Tag saveTag(Tag Tag);

    Tag getTag(Long id);

    Tag getTagByName(String name);

    Page<Tag> listTag(Pageable pageable);

    Tag updateTag(Long id, Tag Tag);

    void deleteTag(Long id);

    List<Tag> listTag();

    List<Tag> getTags(String s);

    List<Tag> listTagTop(Integer size);
}
