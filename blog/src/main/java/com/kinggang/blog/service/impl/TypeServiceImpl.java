package com.kinggang.blog.service.impl;

import com.kinggang.blog.dao.TypeDao;
import com.kinggang.blog.handler.NotFoundException;
import com.kinggang.blog.entity.Type;
import com.kinggang.blog.service.TypeService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author KingGang
 * @className TypeServiceImpl
 * @description
 * @date 2020/9/17 11:48
 */
@Service
public class TypeServiceImpl implements TypeService {

    @Autowired
    TypeDao typeDao;

    @Override
    @Transactional
    public Type saveType(Type type) {
        return typeDao.save(type);
    }

    @Override
    @Transactional
    public Type getType(Long id) {
        return typeDao.getOne(id);
    }

    @Override
    public Type getTypeByName(String name) {
        return typeDao.findByName(name);
    }

    @Override
    @Transactional
    public Page<Type> listTypeTop(Pageable pageable) {
        return typeDao.findAll(pageable);
    }

    @Override
    public List<Type> listTypeTop(Integer size) {
        Sort sort = Sort.by(Sort.Direction.DESC, "blogs.size");
        Pageable pageable = PageRequest.of(0, size, sort);
        return typeDao.findTypeTop(pageable);
    }

    @Override
    @Transactional
    public Type updateType(Long id, Type type) {
        Type t = typeDao.getOne(id);
        if (t == null) {
            throw new NotFoundException("没找到这个类型");
        }
        BeanUtils.copyProperties(type, t);
        return typeDao.save(t);
    }

    @Override
    @Transactional
    public void deleteType(Long id) {
        typeDao.deleteById(id);
    }

    @Override
    public List<Type> listTypeTop() {
        return typeDao.findAll();
    }

}
