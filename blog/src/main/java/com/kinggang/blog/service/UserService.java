package com.kinggang.blog.service;

import com.kinggang.blog.entity.User;

/**
 * @author KingGang
 * @className UserService
 * @description
 * @date 2020/9/17 10:13
 */
public interface UserService {
    User checkUser(String username, String password);
}
