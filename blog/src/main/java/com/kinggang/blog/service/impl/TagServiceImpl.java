package com.kinggang.blog.service.impl;

import com.kinggang.blog.dao.TagDao;
import com.kinggang.blog.handler.NotFoundException;
import com.kinggang.blog.entity.Tag;
import com.kinggang.blog.service.TagService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author KingGang
 * @className TagServiceImpl
 * @description
 * @date 2020/9/17 11:48
 */
@Service
public class TagServiceImpl implements TagService {

    @Autowired
    TagDao tagDao;

    @Override
    @Transactional
    public Tag saveTag(Tag tag) {
        return tagDao.save(tag);
    }

    @Override
    @Transactional
    public Tag getTag(Long id) {
        return tagDao.getOne(id);
    }

    @Override
    public Tag getTagByName(String name) {
        return tagDao.findByName(name);
    }

    @Override
    @Transactional
    public Page<Tag> listTag(Pageable pageable) {
        return tagDao.findAll(pageable);
    }

    @Override
    @Transactional
    public Tag updateTag(Long id, Tag Tag) {
        Tag t = tagDao.getOne(id);
        if (t == null) {
            throw new NotFoundException("没找到这个标签");
        }
        BeanUtils.copyProperties(Tag, t);
        return tagDao.save(t);
    }

    @Override
    @Transactional
    public void deleteTag(Long id) {
        tagDao.deleteById(id);
    }

    @Override
    public List<Tag> listTag() {
        return tagDao.findAll();
    }

    @Override
    public List<Tag> getTags(String ids) {
        return tagDao.findAllById(strToList(ids));
    }

    @Override
    public List<Tag> listTagTop(Integer size) {
        Pageable pageable = PageRequest.of(0, size, Sort.Direction.DESC, "blogs.size");
        return tagDao.findTagTop(pageable);
    }

    //将传过来的 1,3,4 转成List
    private List<Long> strToList(String ids) {
        List<Long> list = new ArrayList<>();
        if (!StringUtils.isEmpty(ids)) {
            String[] split = ids.split(",");
            for (String s : split) {
                list.add(Long.parseLong(s));
            }
        }
        return list;
    }
}
