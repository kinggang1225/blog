package com.kinggang.blog.service.impl;

import com.kinggang.blog.dao.BlogDao;
import com.kinggang.blog.entity.Blog;
import com.kinggang.blog.entity.Type;
import com.kinggang.blog.handler.NotFoundException;
import com.kinggang.blog.service.BlogService;
import com.kinggang.blog.util.MarkdownUtils;
import com.kinggang.blog.vo.BlogQuery;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.persistence.criteria.*;
import java.util.*;

/**
 * @author KingGang
 * @className BlogServiceImpl
 * @description
 * @date 2020/9/17 16:22
 */

@Service
public class BlogServiceImpl implements BlogService {

    @Autowired
    BlogDao blogDao;

    @Override
    public Blog getBlog(Long id) {
        return blogDao.getOne(id);
    }

    //获得Md 格式的博客并转换成html
    @Transactional
    @Override
    public Blog getAndConvert(Long id) {
        Blog blog = blogDao.getOne(id);
        if (blog == null) {
            throw new NotFoundException("博客不存在");
        }
        Blog b = new Blog();
        BeanUtils.copyProperties(blog, b);
        String content = b.getContent();
        b.setContent(MarkdownUtils.markdownToHtmlExtensions(content));
        //浏览次数
        blogDao.updateViews(id);
        return b;
    }

    //动态条件查询
    @Override
    public Page<Blog> listBlog(Pageable pageable, BlogQuery blog) {
        return blogDao.findAll(new Specification<Blog>() {
            @Override
            public Predicate toPredicate(Root<Blog> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
                //查询条件
                List<Predicate> predictate = new ArrayList<>();
                if (!StringUtils.isEmpty(blog.getTitle())) {
                    //判断是否有选择title
                    predictate.add(cb.like(root.<String>get("title"), "%" + blog.getTitle() + "%"));
                }
                if (blog.getTypeId() != null) {
                    //判断是否有分类
                    predictate.add(cb.equal(root.<Type>get("type").get("id"), blog.getTypeId()));
                }
                if (blog.isRecommend()) {
                    //判断是否是推荐的
                    predictate.add(cb.equal(root.<Boolean>get("recommend"), blog.isRecommend()));
                }
                cq.where(predictate.toArray(new Predicate[predictate.size()]));
                return null;
            }
        }, pageable);
    }

    //首页的推荐博客
    @Override
    public List<Blog> listRecommendBlogTop(Integer size) {
        Sort sort = Sort.by(Sort.Direction.DESC, "updateTime");
        Pageable pageRequest = PageRequest.of(0, size, sort);
        List<Blog> blogs = blogDao.listTopBlog(pageRequest);
        return blogs;
    }

    @Override
    @Transactional
    public Blog saveBlog(Blog blog) {
        blog.setCreateTime(new Date());
        blog.setUpdateTime(new Date());
        return blogDao.save(blog);

    }

    @Transactional
    @Override
    public Blog updateBlog(Long id, Blog blog) {
        Blog one = blogDao.getOne(id);
        if (one == null) {
            throw new NotFoundException("博客不存在");
        }
        BeanUtils.copyProperties(blog, one);
        one.setCreateTime(new Date());
        one.setUpdateTime(new Date());
        return blogDao.save(one);
    }

    @Override
    public void deleteBlog(Long id) {
        blogDao.deleteById(id);
    }

    @Override
    public Page<Blog> listBlog(Pageable pageable) {
        return blogDao.findAll(pageable);
    }

    @Override
    public Page<Blog> searchList(String query, Pageable pageable) {
        return blogDao.searchList(query, pageable);
    }

    //根据标签查询博客
    @Override
    public Page<Blog> listBlog(Long id, Pageable pageable) {
        return blogDao.findAll(new Specification<Blog>() {
            @Override
            public Predicate toPredicate(Root<Blog> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
                Join tags = root.join("tags");
                return cb.equal(tags.get("id"), id);
            }
        }, pageable);
    }

    @Override
    public Map<String, List<Blog>> showArchive() {
        Map<String, List<Blog>> map = new HashMap<>();
        //查出所有年份
        List<String> years = blogDao.getYearGroup();
        years.forEach(year -> {
            //按年份查出所有博客
            List<Blog> blogs = blogDao.getBlogByYear(year);
            map.put(year, blogs);
        });
        return map;
    }

    @Override
    public int blogCount() {
        return (int) blogDao.count();
    }
}
