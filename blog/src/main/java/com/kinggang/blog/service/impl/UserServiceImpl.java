package com.kinggang.blog.service.impl;

import com.kinggang.blog.dao.UserDao;
import com.kinggang.blog.entity.User;
import com.kinggang.blog.service.UserService;
import com.kinggang.blog.util.MD5Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author KingGang
 * @className UserServiceImpl
 * @description
 * @date 2020/9/17 10:14
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserDao userDao;

    @Override
    public User checkUser(String username, String password) {
        return userDao.findByUsernameAndPassword(username, MD5Utils.code(password));
    }
}
