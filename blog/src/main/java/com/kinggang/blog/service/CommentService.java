package com.kinggang.blog.service;

import com.kinggang.blog.entity.Comment;

import java.util.List;

/**
 * @author KingGang
 * @className CommentService
 * @description
 * @date 2020/10/1 15:23
 */
public interface CommentService {

    List<Comment> listCommentsByBlogId(Long blogId);

    Comment saveComment(Comment comment);
}
