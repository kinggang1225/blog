package com.kinggang.blog.service.impl;

import com.kinggang.blog.dao.CommentDao;
import com.kinggang.blog.entity.Comment;
import com.kinggang.blog.service.CommentService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author KingGang
 * @className CommentServiceImpl
 * @description 评论
 * @date 2020/10/1 15:25
 */
@Service
public class CommentServiceImpl implements CommentService {

    @Autowired
    CommentDao commentDao;

    @Override
    public List<Comment> listCommentsByBlogId(Long blogId) {
        Sort sort = Sort.by(Sort.Direction.ASC, "createTime");
        List<Comment> comments = commentDao.findByBlogIdAndParentCommentNull(blogId, sort);
        return eachComment(comments);
    }

    @Transactional
    @Override
    public Comment saveComment(Comment comment) {
        Long parentCommentId = comment.getParentComment().getId();
        if (parentCommentId != -1) {
            //不是-1 的就表示存在父级
            comment.setParentComment(commentDao.getOne(parentCommentId));
        } else {
            comment.setParentComment(null);
        }
        comment.setCreateTime(new Date());
        return commentDao.save(comment);
    }

    //循环遍历每个顶级的评论节点
    private List<Comment> eachComment(List<Comment> comments) {
        //将数据放到新的集合中
        List<Comment> commentsView = new ArrayList<>();
        comments.forEach(item -> {
            Comment c = new Comment();
            BeanUtils.copyProperties(item, c);
            commentsView.add(c);
        });
        //合并各层子代到第一层子代集合中
        combineChildren(commentsView);
        return commentsView;
    }

    //临时存放节点的
    private List<Comment> tempReply = new ArrayList<>();

    //组合所有的子节点
    private void combineChildren(List<Comment> comments) {
        comments.forEach(item -> {
            List<Comment> replyComments = item.getReplyComments();
            //循环迭代,找出子代,存放在tempReply中
            replyComments.forEach(this::recursively);
            //将子节点保存
            item.setReplyComments(tempReply);
            //清空临时存放的数据
            tempReply = new ArrayList<>();
        });
    }

    //迭代找出所有子节点
    private void recursively(Comment reply) {
        tempReply.add(reply);
        if (reply.getReplyComments().size() > 0) {
            //存在子节点
            List<Comment> comments = reply.getReplyComments();
            comments.forEach(item -> {
                tempReply.add(item);
                if (item.getReplyComments().size() > 0) {
                    //子节点又存在子节点
                    recursively(item);
                }
            });
        }
    }
}
