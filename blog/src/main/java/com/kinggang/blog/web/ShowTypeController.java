package com.kinggang.blog.web;

import com.kinggang.blog.entity.Type;
import com.kinggang.blog.service.BlogService;
import com.kinggang.blog.service.TypeService;
import com.kinggang.blog.vo.BlogQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

/**
 * @author KingGang
 * @className ShowTypeController
 * @description
 * @date 2020/10/3 9:58
 */
@Controller
public class ShowTypeController {

    @Autowired
    TypeService typeService;

    @Autowired
    BlogService blogService;


    @GetMapping("/types/{id}")
    public String types(@PageableDefault(size = 8, direction = Sort.Direction.DESC, sort = "updateTime") Pageable pageable,
                        @PathVariable("id") Long id, Model model) {
        //获取所有的分类
        List<Type> types = typeService.listTypeTop();
        if (id == -1) {
            //默认进入了分类页
            id = types.get(0).getId();
        }
        //构造查询条件
        BlogQuery blogQuery = new BlogQuery();
        blogQuery.setTypeId(id);
        model.addAttribute("types", types);
        model.addAttribute("page", blogService.listBlog(pageable, blogQuery));
        model.addAttribute("activeTypeId", id);//被选中
        return "types";

    }
}
