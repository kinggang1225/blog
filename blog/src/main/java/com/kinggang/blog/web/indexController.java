package com.kinggang.blog.web;

import com.kinggang.blog.entity.Blog;
import com.kinggang.blog.entity.Type;
import com.kinggang.blog.service.BlogService;
import com.kinggang.blog.service.TagService;
import com.kinggang.blog.service.TypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

/**
 * @author KingGang
 * @className indexController
 * @description
 * @date 2020/9/16 18:27
 */

@Controller
public class indexController {

    @Autowired
    BlogService blogService;

    @Autowired
    TypeService typeService;

    @Autowired
    TagService tagService;

    //首页  并查询出所有的Type 和Tags
    @RequestMapping("/")
    public String index(@PageableDefault(size = 8, sort = {"updateTime"}, direction = Sort.Direction.DESC) Pageable pageable, Model model) {
        Page<Blog> page = blogService.listBlog(pageable);
        model.addAttribute("page", page);
        //查找当前博客对应的tag 和 type
        List<Type> types = typeService.listTypeTop(6);
        model.addAttribute("types",types);
        System.out.println("----------哈哈哈哈哈------------"+ types.toString());
        model.addAttribute("tags", tagService.listTagTop(10));
        model.addAttribute("recommendBlogs", blogService.listRecommendBlogTop(8));
        return "index";
    }

    //博客搜索
    @PostMapping("/search")
    public String search(@PageableDefault(size = 8, direction = Sort.Direction.DESC,
            sort = "updateTime") Pageable pageable, @RequestParam("query") String query, Model model) {
        //返回查询结果
        model.addAttribute("page", blogService.searchList("%" + query + "%", pageable));
        //同时返回查询条件展示
        model.addAttribute("query", query);
        return "search";
    }

    @GetMapping("/blog/{id}")
    public String blog(@PathVariable("id") Long id, Model model) {
        Blog blog = blogService.getAndConvert(id);
        model.addAttribute("blog", blog);
        return "blog";
    }

    @GetMapping("/about")
    public String about() {
        return "about";
    }

    //页脚展示
    @GetMapping("/footer/newblog")
    public String newBlogList(Model model) {
        model.addAttribute("newblogs", blogService.listRecommendBlogTop(3));
        return "_fragments::newblogList";
    }
}
