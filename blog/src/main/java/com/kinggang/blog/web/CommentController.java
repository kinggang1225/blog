package com.kinggang.blog.web;

import com.kinggang.blog.entity.Comment;
import com.kinggang.blog.entity.User;
import com.kinggang.blog.service.BlogService;
import com.kinggang.blog.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpSession;

/**
 * @author KingGang
 * @className CommentController
 * @description 评论
 * @date 2020/10/1 15:09
 */
@Controller
public class CommentController {
    @Autowired
    CommentService commentService;

    @Autowired
    BlogService blogService;

    @Value("${comment.avatar}")
    private String avatar;

    //处理评论页面展示
    @GetMapping("/comments/{blogId}")
    public String commentsList(@PathVariable("blogId") Long blogId, Model model) {
        model.addAttribute("comments", commentService.listCommentsByBlogId(blogId));
        return "blog::commentList";
    }

    //新增评论
    @PostMapping("/comments")
    public String postComments(Comment comment, HttpSession session) {
        Long blogId = comment.getBlog().getId();
        comment.setBlog(blogService.getBlog(blogId));
        User user = (User) session.getAttribute("user");
        if (user != null) {
            //是管理员回复
            comment.setAvatar(user.getAvatar());
            comment.setAdminComment(true);
//            comment.setNickname(user.getNickname());
        } else {
            //普通访客
            comment.setAvatar(avatar);
        }
        commentService.saveComment(comment);
        return "redirect:/comments/" + comment.getBlog().getId();
    }
}
