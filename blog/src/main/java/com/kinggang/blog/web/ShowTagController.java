package com.kinggang.blog.web;

import com.kinggang.blog.entity.Tag;
import com.kinggang.blog.service.BlogService;
import com.kinggang.blog.service.TagService;
import com.kinggang.blog.vo.BlogQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

/**
 * @author KingGang
 * @className ShowTypeController
 * @description
 * @date 2020/10/3 9:58
 */
@Controller
public class ShowTagController {

    @Autowired
    TagService tagService;

    @Autowired
    BlogService blogService;


    @GetMapping("/tags/{id}")
    public String types(@PageableDefault(size = 8, direction = Sort.Direction.DESC, sort = "updateTime") Pageable pageable,
                        @PathVariable("id") Long id, Model model) {
        //获取所有的分类
        List<Tag> tags = tagService.listTag();
        if (id == -1) {
            //默认进入了分类页
            id = tags.get(0).getId();
        }
        model.addAttribute("tags", tags);
        model.addAttribute("page", blogService.listBlog(id,pageable));
        model.addAttribute("activeTagId", id);//被选中
        return "tags";

    }
}
