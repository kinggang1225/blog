package com.kinggang.blog.web.admin;

import com.kinggang.blog.entity.Blog;
import com.kinggang.blog.entity.User;
import com.kinggang.blog.service.BlogService;
import com.kinggang.blog.service.TagService;
import com.kinggang.blog.service.TypeService;
import com.kinggang.blog.vo.BlogQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;

/**
 * @author KingGang
 * @className BlogController
 * @description
 * @date 2020/9/17 16:33
 */
@RequestMapping("/admin")
@Controller
public class BlogsController {

    private static final String LIST = "admin/blogs";
    private static final String INPUT = "admin/blogs-input";
    private static final String REDIRECT_LIST = "redirect:/admin/blogs";


    @Autowired
    BlogService blogService;

    @Autowired
    TypeService typeService;

    @Autowired
    TagService tagService;

    private void setTagAndType(Model model) {
        model.addAttribute("tags", tagService.listTag());
        model.addAttribute("types", typeService.listTypeTop());
    }

    //获取所有博客文章
    @GetMapping("/blogs")
    public String blogs(@PageableDefault(size = 5, sort = {"updateTime"}, direction = Sort.Direction.DESC) Pageable pageable, BlogQuery blog, Model model) {
        model.addAttribute("page", blogService.listBlog(pageable, blog));
        model.addAttribute("types", typeService.listTypeTop());
        return LIST;
    }

    //搜索文章
    @PostMapping("/blogs/search")
    public String blogsSearch(@PageableDefault(size = 5, sort = {"updateTime"}, direction = Sort.Direction.DESC) Pageable pageable, BlogQuery blog, Model model) {
        model.addAttribute("page", blogService.listBlog(pageable, blog));
        model.addAttribute("types", typeService.listTypeTop());
        return "admin/blogs::blogList";
    }

    //新建博客页
    @GetMapping("/blogs/input")
    public String input(Model model) {
        setTagAndType(model);
        model.addAttribute("blog", new Blog());
        return INPUT;
    }

    //修改
    @GetMapping("/blogs/{id}/input")
    public String editInput(@PathVariable("id") Long id, Model model) {
        setTagAndType(model);
        Blog blog = blogService.getBlog(id);
        blog.init();
        model.addAttribute("blog", blog);
        return INPUT;
    }

    //处理博客新建
    @PostMapping("/blogs")
    public String post(Blog blog, HttpSession session, Model model, RedirectAttributes attributes) {
        User user = (User) session.getAttribute("user");
        blog.setType(typeService.getType(blog.getType().getId()));
        blog.setTags(tagService.getTags(blog.getTagIds()));
        blog.setViews(0);
        blog.setUser(user);
        Blog b;
        if (blog.getId() == null) {
            //保存博客
            b = blogService.saveBlog(blog);
        } else {
            //更新博客
            b = blogService.updateBlog(blog.getId(), blog);
        }
        if (b != null) {
            attributes.addFlashAttribute("message", "操作成功");
        } else {
            attributes.addFlashAttribute("message", "操作失败");
        }


        return REDIRECT_LIST;
    }

    //删除博客
    @GetMapping("/blogs/{id}/delete")
    public String delete(@PathVariable("id") Long id, RedirectAttributes attributes) {
        blogService.deleteBlog(id);
        attributes.addFlashAttribute("message", "删除成功");
        return REDIRECT_LIST;
    }

}
