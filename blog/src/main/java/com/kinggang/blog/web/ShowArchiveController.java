package com.kinggang.blog.web;

import com.kinggang.blog.entity.Blog;
import com.kinggang.blog.service.BlogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;
import java.util.Map;

/**
 * @author KingGang
 * @className ShowArchiveController
 * @description 归档页面
 * @date 2020/10/3 11:06
 */
@Controller
public class ShowArchiveController {

    @Autowired
    BlogService blogService;


    //按年份查询你所有博客并归档
    @GetMapping("/archives")
    public String archive(Model model) {
        Map<String, List<Blog>> showArchive = blogService.showArchive();
        model.addAttribute("archiveMap", showArchive);
        model.addAttribute("blogCount", blogService.blogCount());
        return "archives";
    }
}
