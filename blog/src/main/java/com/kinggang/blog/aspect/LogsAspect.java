package com.kinggang.blog.aspect;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Date;

/**
 * @author KingGang
 * @className LogsAspect
 * @description 日志监控
 * @date 2020/9/16 19:08
 */

@Aspect
@Component
@Slf4j
public class LogsAspect {

    @Autowired
    private HttpServletRequest request;

    private Date visitTime;//执行时间
    private Class aClass;//执行的是哪个类
    private Method method;//执行的是哪个方法
    private String ip;
    private String url;

    @Pointcut(value = "execution(* com.kinggang.blog.web..*.*(..))")
    public void log() {
    }

    //前置通知，主要获取开始执行时间，执行的是哪个类，哪个方法
    @Before(value = "log()")
    public void doBefore(JoinPoint jp) throws NoSuchMethodException {
        log.info("------------doBefore----------");
        url = String.valueOf(request.getRequestURL());
        ip = request.getRemoteAddr();
        visitTime = new Date();
        aClass = jp.getTarget().getClass();

        //获取到具体的Method对象
        String methodName = jp.getSignature().getName();//获取访问的方法名字
        Object[] args = jp.getArgs();//获取到方法的所有参数
//        if (args == null || args.length == 0) {
//            //方法无参
//            method = aClass.getMethod(methodName);//根据方法名获取方法
//        } else {
//            //方法有不确定数目的参数
//            Class[] argsClass = new Class[args.length];//定义装一个参数类的数组
//            for (int i = 0; i < args.length; i++) {
//                argsClass[i] = args[i].getClass();//将每个参数的类到数组中
//            }
//            method = aClass.getMethod(methodName, argsClass);//将方法的参数的类和方法名封装到要获取的方法中
//        }

        log.info("---开始时间:{} 执行的类名:{} 执行的方法名:{} 方法参数:{} 请求URL:{} 请求IP:{}---",
                visitTime, aClass.getCanonicalName(), methodName, Arrays.toString(args), url, ip);

    }

    @After(value = "log()")
    public void doAfter() {
        log.info("------------doAfter----------");
        //获取访问时长
        long time = new Date().getTime() - visitTime.getTime();
        log.info("---结束时间:{} 共执行时长:{} ms ---", new Date(), time);

    }

    //方法最终返回值
    @AfterReturning(returning = "result", pointcut = "log()")
    public void doAfterReturn(Object result) {
        log.info("------------返回数据:{}----------", result.toString());
    }


}
